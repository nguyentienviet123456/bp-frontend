import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { omit, omitBy, isUndefined, range } from 'lodash'

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() quantity: number
  @Input() total: number
  @Input() page: number
  @Input() length: number
  @Input() totalPages: number
  @Input() autoNavigate = true
  @Input() noQuantitySelector = false
  @Input() isSm: boolean
  @Input() useLink = true
  @Input() root = '.'
  @Input() showInfo = true
  @Input() numberOfShowPages = 3

  @Output() change = new EventEmitter()

  pages: Array<number>
  params

  get start(): number {
    return (this.page - 1) * this.quantity + 1
  }

  get end(): number {
    return (this.page - 1) * this.quantity + this.length
  }

  constructor(private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnChanges() {
    this.prepareDisplayPages()
  }

  ngOnInit() {
    this.route.params
      .subscribe((params) => {
        this.params = params
      })
    this.prepareDisplayPages()
  }

  navigate(params) {
    const currentParams = this.route.snapshot.params
    params = omit(omitBy(Object.assign({}, currentParams, params), isUndefined), ['id'])
    if (!this.autoNavigate) {
      this.change.emit(params)
      return
    }
    this.router.navigate([params], {relativeTo: this.route})
  }

  getLink(page: number) {
    if (!this.autoNavigate) {
      return
    }
    return omit(Object.assign({}, this.route.snapshot.params, {page}), ['id'])
  }

  private prepareDisplayPages() {
    const distance = Math.floor(this.numberOfShowPages / 2)
    const start = Math.max(this.page - distance, 1)
    const end = Math.min(start + this.numberOfShowPages - 1, this.totalPages)
    let displayPages = range(start, end + 1)
    if (!displayPages.length) {
      displayPages = [1]
    }
    this.pages = displayPages
  }
}

