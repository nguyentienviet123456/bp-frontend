import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from 'src/app/services/api/auth.service';
import { of } from 'rxjs';

@Injectable()
export class MainGuard implements CanActivate {

    token: string
    constructor(private authService: AuthService, private router: Router) {
        this.token = this.authService.getToken();
      }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        if (this.token) {
            return of(true)
        } else {
            this.navigate('homepage')
            return of(false)
        }
      }

      private navigate(router: string) {
        this.router.navigate(['/'+ router]);
      }
}
