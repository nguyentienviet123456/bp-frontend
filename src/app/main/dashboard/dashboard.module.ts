import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DashboardComponent } from 'src/app/main/dashboard/dashboard.component';
import { DashboardRoutingModule } from 'src/app/main/dashboard/dashboard-routing.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  // providers: [
  //   {
  //     provide: LocationStrategy,
  //     useClass: HashLocationStrategy
  //   },
  //   {
  //     provide: HTTP_INTERCEPTORS,
  //     useClass: TokenInterceptor,
  //     multi: true
  //   },
  // ],
})
export class DashboardModule { }
