import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { PluginService } from 'src/app/services/api/plugin.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  collapseOne = false;
  collapseTwo = false;
  constructor(
    private pluginService: PluginService
  ) { }

  ngOnInit() {

  }

  testFunction(){
    console.log('test')
    const next = data => {
      console.log(data)
    }

    const error = reason => {
     
    }
    this.pluginService.plugin.get('abc').subscribe(next,error);
  }
}
