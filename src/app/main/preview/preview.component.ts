import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/api/user.service';
import { User } from 'types/viewmodels';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Alert } from '../../services/alert';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  page = 1
  quantity = 10
  totalPages: number
  total: number
  users: User[]
  isLoading: boolean
  form: FormGroup
  clickedUpdate: boolean = false
  clickedUserId: string
  clickedUser: User

  constructor(
    private userApi: UserService,
    private fb: FormBuilder,
    private alert: Alert,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.fetch(1)
    this.form = this.fb.group({
      email: [undefined],
      password: [undefined],
      name: [undefined],
      picture: [undefined],
      role: [undefined]
    })
  }

  onChangePage(ev) {
    this.fetch(ev.page)
  }

  private fetch(page) {
    this.spinner.show();
    this.queryUsers(this.quantity, page)
  }

  private queryUsers(limit: number, page: number) {
    this.userApi.user.getMany({page, limit}).subscribe(res => {
      this.spinner.hide();
      this.page = page;
      this.users = res.entity.rows;
      this.total = res.entity.count;
      this.totalPages = Math.ceil(this.total / this.quantity)
    }, () => {
      this.isLoading = false;
    })
  }

  private createUserUsingEmail() {
    const next = user => {
      this.toastr.success('create user successfully ~')
      this.fetch(1)
      this.form.reset()
    }

    const error = reason => {
      this.toastr.error(reason.message)
    }
    this.form.value.password = 'Anhviet1'
    this.userApi.user.create(this.form.value).subscribe(next,error)
  }

  private deleteUser(id: string) {
    const next = user => {
      this.fetch(1)
      this.toastr.success('delete user successfully!')
     }
 
     const error = reason => {
      this.toastr.error(reason.message)
     }
 
     this.userApi.user.delete(id).subscribe(next,error)
  }

  private clickedUserRow(user: User) {
    this.clickedUser = user
    this.clickedUpdate = true;
    this.form.patchValue({name: this.clickedUser.name})
  }

  private updateUser() {
    const next = user => {
      this.fetch(1)
      this.toastr.success('update user successfully!')
      this.form.reset()
     }
 
     const error = reason => {
      this.toastr.error(reason.message || 'cannot update user!')
     }
    this.userApi.user.update(this.clickedUserId, {name: this.form.value.name, picture: this.clickedUser.picture}).subscribe(next, error)
  }
}
