import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';
import { PluginUser } from 'types/viewmodels';

@Component({
  selector: 'app-plugin-reference',
  templateUrl: './plugin-reference.component.html',
  styleUrls: ['./plugin-reference.component.css']
})
export class PluginReferenceComponent implements OnInit {

  references = ['Config', 'Data', 'Member']
  selectedReference: number
  pluginUserId: string
  form: FormGroup
  pluginName: string
  pluginUser: PluginUser

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private pluginUserApi: PluginUserService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.selectedReference = 0
    this.pluginUserId = this.route.snapshot.params.pluginUserId
    this.form = this.fb.group({
      name: [undefined],
    })
    this.spinner.show()
    this.pluginUserApi.pluginUser.get(this.pluginUserId).subscribe(res => {
      this.spinner.hide()
      this.pluginUser = res.entity
      this.pluginName = res.entity.name || res.entity.plugin.name
      this.form.patchValue({name: this.pluginName})
    }, error => {
      this.spinner.hide()
    })
  }

  private changeName() {
    this.spinner.show()
    const next = data => {
      this.spinner.hide()
      this.toast.success('update name successfully!')
      this.form.patchValue({name: this.form.value.name})
      this.pluginName = this.form.value.name
    }
    const error = reason => {
      this.spinner.hide()
      this.toast.error(reason.message || 'fail to name!')
    }
    this.pluginUserApi.pluginUser.update(this.pluginUserId, {name: this.form.value.name, group: this.pluginUser.group}).subscribe(next, error);
  }
}
