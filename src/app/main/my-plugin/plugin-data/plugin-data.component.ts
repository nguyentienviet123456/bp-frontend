import { Component, OnInit } from '@angular/core';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-plugin-data',
  templateUrl: './plugin-data.component.html',
  styleUrls: ['./plugin-data.component.css']
})
export class PluginDataComponent implements OnInit {

  pluginUserId: string

  constructor(
    private pluginUserApi: PluginUserService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.pluginUserId = this.route.snapshot.params.pluginUserId
  }

}
