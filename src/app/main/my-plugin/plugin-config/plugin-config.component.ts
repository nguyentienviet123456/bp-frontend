import { Component, OnInit } from '@angular/core';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-plugin-config',
  templateUrl: './plugin-config.component.html',
  styleUrls: ['./plugin-config.component.css']
})
export class PluginConfigComponent implements OnInit {

  pluginUserId: string

  constructor(
    private pluginUserApi: PluginUserService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.pluginUserId = this.route.snapshot.params.pluginUserId
  }

}
