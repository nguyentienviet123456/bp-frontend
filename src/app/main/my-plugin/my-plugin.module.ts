import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyPluginComponent } from './my-plugin.component';
import { MyPluginRoutingModule } from './my-plugin-routing.module';
import { PluginUserComponent } from './plugin-user/plugin-user.component';
import { PluginMemberComponent } from './plugin-member/plugin-member.component';
import { PluginDataComponent } from './plugin-data/plugin-data.component';
import { PluginConfigComponent } from './plugin-config/plugin-config.component';
import { CollapseModule, BsDropdownModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PluginReferenceComponent } from './plugin-reference/plugin-reference.component';

@NgModule({
  declarations: [
    MyPluginComponent, 
    PluginUserComponent,
    PluginMemberComponent, 
    PluginDataComponent, 
    PluginConfigComponent, PluginReferenceComponent
  ],
  imports: [
    CommonModule,
    MyPluginRoutingModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    DragDropModule
  ]
})
export class MyPluginModule { }
