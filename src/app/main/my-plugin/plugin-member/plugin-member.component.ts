import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Member } from 'types/viewmodels';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';

@Component({
  selector: 'app-plugin-member',
  templateUrl: './plugin-member.component.html',
  styleUrls: ['./plugin-member.component.css']
})
export class PluginMemberComponent implements OnInit {

  form: FormGroup
  pluginUserId: string
  members: Member[]

  constructor(
    private pluginUserApi: PluginUserService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.pluginUserId = this.route.snapshot.params.pluginUserId
    this.form = this.fb.group({
      email: [undefined],
    })
    this.refreshMembers()
  }

  refreshMembers() {
    this.pluginUserApi.pluginUser.get(this.pluginUserId).subscribe(res => {
      this.members = res.entity.members
    })
  }

  createMember() {
    this.spinner.show()
    const next = data => {
      this.spinner.hide()
      this.toast.success('create member successfully!')
      this.refreshMembers()
    }
    const error = reason => {
      this.spinner.hide()
      this.toast.error(reason.message || 'fail to create member~')
    }

    this.pluginUserApi.pluginUser.addMember(this.pluginUserId, {email: this.form.value.email}).subscribe(next, error)
  }

  deleteMember(id: string) {
    this.spinner.show()
    const next = data => {
      this.spinner.hide()
      this.toast.success('delete member successfully!')
      this.refreshMembers()
    }
    const error = reason => {
      this.spinner.hide()
      this.toast.error(reason.message || 'fail to delete member!')
    }

    this.pluginUserApi.pluginUser.deleteMember(this.pluginUserId, id).subscribe(next, error)
  }

  changeRoleMember(role: string, id: string) {
    this.members.forEach((member) => {
      if (member.id === id) {
        member.role = role
      }
    })
    this.spinner.show()
    const next = data => {
      this.spinner.hide()
      this.toast.success('update member successfully!')
      this.refreshMembers()
    }
    const error = reason => {
      this.spinner.hide()
      this.toast.error(reason.message || 'fail to update member!')
    }


    this.pluginUserApi.pluginUser.update(this.pluginUserId, {members: this.members}).subscribe(next, error)
  }
}
