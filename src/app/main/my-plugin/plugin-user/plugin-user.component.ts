import { Component, OnInit } from '@angular/core';
import { PluginUser, Group } from 'types/viewmodels';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder } from '@angular/forms';
import { group } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';
import { GroupService } from 'src/app/services/api/group.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-plugin-user',
  templateUrl: './plugin-user.component.html',
  styleUrls: ['./plugin-user.component.css']
})
export class PluginUserComponent implements OnInit {

  pluginUsers: PluginUser[]
  form: FormGroup
  search: FormGroup
  selectedGroupId: string
  groups: Group[]
  itemCollapse: number
  tempPluginUser: PluginUser[]

  constructor(
    private pluginUserApi: PluginUserService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private groupApi: GroupService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.listenRoute()
    this.form = this.fb.group({
      name: [undefined],
    })

    this.search = this.fb.group({
      query: [undefined],
    })
  }

  private listenRoute() {
    this.route.data.subscribe((data) => {
      this.groups = data.groups.entity.rows
      this.pluginUsers = data.pluginUsers.entity
      this.tempPluginUser = cloneDeep(this.pluginUsers)
      this.mappingGroupPluginUser(this.groups, this.pluginUsers)
    })
  }

  private mappingGroupPluginUser(groups: Group[], pluginUsers: PluginUser[]) {
    groups.sort(function(a, b){return a.weight - b.weight});
    groups.forEach((group, index) => {
      // if (!group.weight) {
      //   group.weight = index
      //   this.groupApi.group.update(group.id, {weight: index}).subscribe(res => {
      //   }, error => {
      //   })
      // }
      group.weight = index
      group.pluginUsers = []
      pluginUsers.map((pluginUser, index) => {
        if (pluginUser.group == group.id) {
          group.pluginUsers.push(pluginUser)
        }
      })
    })
    this.pluginUsers = this.pluginUsers.filter(item => !item.group )
  }

  private refreshGroups() {
    this.groupApi.group.getMany({}).subscribe(res => {
      this.groups = res.entity.rows
      this.mappingGroupPluginUser(this.groups, this.tempPluginUser)
    }, () => {
    })
  }

  private refreshPluginUsers() {
    this.pluginUserApi.pluginUser.getMany({}).subscribe(res => {
      this.pluginUsers = res.entity
      this.tempPluginUser = cloneDeep(this.pluginUsers)
      this.mappingGroupPluginUser(this.groups, this.pluginUsers)
    }, () => {
    })
  }

  private addNewGroup() {
    const next = data => {
      this.refreshGroups()
      this.toastr.success("add new group successfully!")
    }
    const error = reason => {
      this.toastr.error( reason.message ||"fail to add new group !")
    }

    this.groupApi.group.create({name: 'New Group'}).subscribe(next, error);
  }

  private updateGroup(id: string) {
    const next = data => {
      this.toastr.success("update group successfully!")
      this.refreshGroups()
    }
    const error = reason => {
      this.toastr.error( reason.message ||"fail to update group !")
    }

    this.groupApi.group.update(id, {name: this.form.value.name}).subscribe(next, error);
  }

  private deleteGroup(id: string) {
    const next = data => {
      this.refreshGroups()
      this.toastr.success("delete group successfully!")
    }
    const error = reason => {
      this.toastr.error( reason.message ||"fail to delete group !")
    }
    this.groupApi.group.delete(id).subscribe(next, error);
  }

  private updateForm(name: string) {
    this.form.patchValue({name: name})
  }

  private deletePluginUser(id: string) {
    const next = data => {
      this.toastr.success("delete pluginuser successfully!")
      this.refreshPluginUsers()
    }
    const error = reason => {
      this.toastr.error( reason.message ||"fail to delete pluginuser !")
    }
    this.pluginUserApi.pluginUser.delete(id).subscribe(next, error);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
      if (event.container.id == 'list-pluginuser') {
        event.container.id = null
      }
      const next = data => {
        this.toastr.success("update pluginuser successfully!")
      }
      const error = reason => {
        this.toastr.error( reason.message ||"fail to update pluginuser !")
      }
      this.pluginUserApi.pluginUser.update(event.item.data.id, {group: event.container.id, name: event.item.data.name}).subscribe(next, error);
    }
  }

  private onSearch() {
    this.pluginUserApi.pluginUser.getMany({q: this.search.value.query}).subscribe(res => {
      console.log(res)
    })
  }
}
