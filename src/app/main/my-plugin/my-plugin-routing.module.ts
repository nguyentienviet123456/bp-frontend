import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyPluginComponent } from './my-plugin.component';
import { PluginUserComponent } from './plugin-user/plugin-user.component';
import { PluginMemberComponent } from './plugin-member/plugin-member.component';
import { PluginDataComponent } from './plugin-data/plugin-data.component';
import { PluginConfigComponent } from './plugin-config/plugin-config.component';
import { PluginUserResolverService } from '../../services/resolver/plugin-user-resolver.service';
import { GroupResolverService } from '../../services/resolver/group-resolver.service';
import { PluginReferenceComponent } from './plugin-reference/plugin-reference.component';

const routes: Routes = [
  {
    path: '',
    component: MyPluginComponent,
    children: [
      {
        path: '',
        redirectTo: 'plugin-user',
      },
      {
        path: 'plugin-user',
        component: PluginUserComponent,
        resolve: {
          pluginUsers: PluginUserResolverService,
          groups: GroupResolverService
        }
      },
      {
        path: ':pluginUserId',
        component: PluginReferenceComponent,
        children: [
          {
            path: '',
            redirectTo: 'config'
          },
          {
            path: 'member',
            component: PluginMemberComponent,
          },
          {
            path: 'data',
            component: PluginDataComponent,
          },
          {
            path: 'config',
            component: PluginConfigComponent,
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPluginRoutingModule { }
