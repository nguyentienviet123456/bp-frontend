import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { PreviewComponent } from './preview/preview.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MainGuard } from 'src/app/main/main.guard';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'homepage',
      },
      {
        path: 'homepage',
        component: HomepageComponent,
      },
      {
        path: 'my-plugin',
        loadChildren: './my-plugin/my-plugin.module#MyPluginModule',
        canActivate: [MainGuard]
      },
      {
        path: 'all-plugin',
        loadChildren: './all-plugin/all-plugin.module#AllPluginModule',
        canActivate: [MainGuard]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    MainGuard,
  ]
})
export class MainRoutingModule { }
