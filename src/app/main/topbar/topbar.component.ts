import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/api/auth.service';
import { environment } from '../../../environments/environment';

declare const FB: any;

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  location: any
  token: string
  accessTokenFB: string
  isLogout: boolean

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { 
    
  }

  ngOnInit() {
    this.location = this.router.url;
    this.token = this.authService.getToken();
    this.isLogout = this.token == null ? true : false;
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : environment.tokenFB,
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

     this.authService.change.subscribe(isLogout => {
      this.isLogout = isLogout
    });
  }

  submitLogin(){
    FB.login((response)=>
        {
          if (response.authResponse)
          {
            this.isLogout = false;
            this.accessTokenFB = response.authResponse.accessToken
            this.authService.auth.facebook({access_token : this.accessTokenFB}).subscribe(res => {
              localStorage.setItem('jwtToken', res.entity.token)
              localStorage.setItem('user', JSON.stringify(res.entity.user))
            }, error =>  {

            })
            this.router.navigate(['/all-plugin']);
           }
           else
           {
         }
      }, {scope: 'email'});
  }

  logout() {
    this.authService.logout()
    this.authService.toggle()
  }
}
