import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllPluginComponent } from './all-plugin.component';

const routes: Routes = [
  {
    path: '',
    component: AllPluginComponent,
    children: [
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllPluginRoutingModule { }
