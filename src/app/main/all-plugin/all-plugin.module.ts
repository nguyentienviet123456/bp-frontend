import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllPluginRoutingModule } from 'src/app/main/all-plugin/all-plugin-routing.module';
import { AllPluginComponent } from 'src/app/main/all-plugin/all-plugin.component';

@NgModule({
  declarations: [
    AllPluginComponent
  ],
  imports: [
    CommonModule,
    AllPluginRoutingModule
  ]
})
export class AllPluginModule { }
