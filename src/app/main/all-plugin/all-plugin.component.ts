import { Component, OnInit } from '@angular/core';
import { BpPlugin } from 'types/viewmodels';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PluginUserService } from 'src/app/services/api/plugin-user.service';
import { PluginService } from 'src/app/services/api/plugin.service';

@Component({
  selector: 'app-all-plugin',
  templateUrl: './all-plugin.component.html',
  styleUrls: ['./all-plugin.component.css']
})
export class AllPluginComponent implements OnInit {

  plugins: BpPlugin[]
  isLoading: boolean
  selectedPlugin: BpPlugin
  
  constructor(
    private pluginApi: PluginService,
    private pluginUserApi: PluginUserService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.isLoading = true
    this.pluginApi.plugin.getMany().subscribe(res => {
      this.isLoading = false
      this.plugins = res.entity
    },() => {
      this.isLoading = false
    })
  }

  createPluginUser(plugIn: BpPlugin) {
    this.spinner.show();
    const next = pluginUser => {
      this.spinner.hide();
      this.router.navigate(['/my-plugin'])
      this.toastr.success('create plugin user successfully ~')
    }

    const error = reason => {
      this.spinner.hide();
      this.toastr.error(reason.message)
    }
    this.pluginUserApi.pluginUser.create({pkg_id: plugIn.pkgs[0]._id, plugin_id: plugIn.id}).subscribe(next, error);
  }
}
