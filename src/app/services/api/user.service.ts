import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from './baseUrl';
import { BaseApiService } from 'base-classes';
import { User, QueryResult } from 'types/viewmodels';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseApiService {

  constructor(httpClient: HttpClient, @Inject(baseUrl) protected hostUrl: string) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api');
  }

  user = {
    create: ( command: {
      email: string,
      password: string,
      name: string,
      picture: string,
      role: string,
    }) => this.httpClient.post(this.createUrl('users'), command),
    delete: (id: string) => this.httpClient.delete(this.createUrl(`users/${id}`)),
    get: (id: string) => this.httpClient.get<User>(this.createUrl(`users/${id}`)),
    getMany: (command: {
        page: number,
        limit: number,
    }) => this.httpClient.get<QueryResult<User>>(this.createUrl('users'),  { params: this.createParams(command) }),
    // }) => this.httpClient.get<QueryResult<User>>(this.createUrl('users')),
    update: (id: string, command: {
      name: string,
      picture: string,
    }) => this.httpClient.put(this.createUrl(`users/${id}`), command),
    updatePassword: (id: string, command: {
        password: string,
      }) => this.httpClient.put(this.createUrl(`users/${id}/password`), command),
  };
}
