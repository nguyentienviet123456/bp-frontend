import { InjectionToken } from '@angular/core';
import { environment } from '../../../environments/environment';

export const baseUrl = new InjectionToken<string>('baseUrl', {
  providedIn: 'root',
  factory: () => environment.apiHostUrl
});
