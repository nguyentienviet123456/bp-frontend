import { Injectable, Inject } from '@angular/core';
import { BaseApiService } from 'base-classes';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from './baseUrl';
import { GetAllResult, Group, QueryResult } from 'types/viewmodels';

@Injectable({
  providedIn: 'root'
})
export class GroupService extends BaseApiService {

  constructor(httpClient: HttpClient, @Inject(baseUrl) protected hostUrl: string) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api');
  }

  group = {
    create: (command) => this.httpClient.post(this.createUrl('groups'), command),
    getMany: (params) => this.httpClient.get<QueryResult<Group>>(this.createUrl(`groups`), { params: this.createParams(params) }),
    update: (id: string, command) => this.httpClient.put(this.createUrl(`groups/${id}`), command),
    delete: (id: string) => this.httpClient.delete(this.createUrl(`groups/${id}`)),
  };
  
}
