import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from './baseUrl';
import { BaseApiService } from 'base-classes';
import { GetAllResult, BpPlugin } from 'types/viewmodels';

@Injectable({
  providedIn: 'root'
})
export class PluginService extends BaseApiService {

  constructor(httpClient: HttpClient, @Inject(baseUrl) protected hostUrl: string) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api');
  }

  plugin = {
    create: ( command: {
      access_token: string,
      name: string,
      type: string,
      desc: string,
      targets: string,
      bg_color: string,
      bg_img: string,
      pkgs: any
    }) => this.httpClient.post(this.createUrl('plugins'), command),
    delete: (pluginId: string) => this.httpClient.delete(this.createUrl(`plugins/${pluginId}`)),
    get: (pluginId: string) => this.httpClient.get<BpPlugin>(this.createUrl(`plugins/${pluginId}`)),
    getMany: () => this.httpClient.get<GetAllResult<BpPlugin>>(this.createUrl('plugins')),
    update: (pluginId: string, command: {
      access_token: string,
      name: string,
      type: string,
      desc: string,
      targets: string,
      bg_color: string,
      bg_img: string,
      pkgs: any
    }) => this.httpClient.put(this.createUrl(`plugins/${pluginId}`), command),
  };
}
