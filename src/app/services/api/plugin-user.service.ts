import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from './baseUrl';
import { BaseApiService } from 'base-classes';
import { GetAllResult, QueryResult, PluginUser, GetResult } from 'types/viewmodels';

@Injectable({
  providedIn: 'root'
})
export class PluginUserService extends BaseApiService {

  constructor(httpClient: HttpClient, @Inject(baseUrl) protected hostUrl: string) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api');
  }

  pluginUser = {
    create: ( command: {
      plugin_id: string,
      pkg_id: string,
    }) => this.httpClient.post<PluginUser>(this.createUrl('pluginusers'), command),
    delete: (id: string) => this.httpClient.delete(this.createUrl(`pluginusers/${id}`)),
    get: (id: string) => this.httpClient.get<GetResult<PluginUser>>(this.createUrl(`pluginusers/${id}`)),
    getMany: (params) => this.httpClient.get<GetAllResult<PluginUser>>(this.createUrl('pluginusers'), {params: this.createParams(params)}),
    update: (id: string, command: any) => this.httpClient.put(this.createUrl(`pluginusers/${id}`), command),
    data: (id: string, params: {
      page: number,
      limit: number,
    }) => this.httpClient.get<QueryResult<PluginUser>>(this.createUrl(`pluginusers/${id}/data`),  { params: this.createParams(params) }),
    addMember: (id: string, command: {
      email: string
    }) => this.httpClient.put(this.createUrl(`pluginusers/${id}/members`), command),
    deleteMember: (id: string, memberId: string)  => this.httpClient.delete(this.createUrl(`pluginusers/${id}/members/${memberId}`), {}),
  };
}
