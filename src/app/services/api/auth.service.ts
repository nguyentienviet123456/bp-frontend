import { Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { BaseApiService } from 'base-classes';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { baseUrl } from 'src/app/services/api/baseUrl';
import { GetResult, AuthResult } from 'types/viewmodels';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseApiService {
  cachedRequests: Array<HttpRequest<any>> = [];
 
  isLogout: boolean
  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(
    httpClient: HttpClient, 
    @Inject(baseUrl) protected hostUrl: string,
    private router: Router
  ) 
    {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api');
  }

  auth = {
    facebook:(command) => this.httpClient.post<GetResult<AuthResult>>(this.createUrl('auth/facebook'), command),
  };

  public getToken(): string {
    return localStorage.getItem('jwtToken');
  }

  public logout() {
    localStorage.removeItem('jwtToken');
    this.router.navigate(['/homepage']);
  }

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return this.tokenNotExpired(token);
  }
  public tokenNotExpired(token) {
    if(token) {
      var jwtHelper = new JwtHelperService();
      return token != null && !jwtHelper.isTokenExpired(token);
    }else {
      return false;
    }
    
  }
  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
  }

  toggle() {
    this.isLogout = true;
    this.change.emit(this.isLogout);
  }
}
