import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BackendError } from 'types/interfaces';
import { AuthService } from 'src/app/services/api/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
  public static currentTeamId : any;
  constructor(public auth: AuthService) {
   }

   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    let requestOption:any = {};
     
    if(this.auth.getToken()) {
      requestOption.setParams = {
        access_token: `${this.auth.getToken()}`
      }
    } 

    request = request.clone(requestOption); 
    return next.handle(request)
    .pipe(catchError((response, retry) => {
      if (this.isUnauthorized(response)) {
        return throwError(<BackendError> {
          silent: true
        })
      } else if (response instanceof HttpErrorResponse && response.status === 403) {
        return throwError({
          messages: ['You do not have permission']
        })
      }
      return throwError(response.error || response)
    }))
  }

  private isUnauthorized(err) {
    return err instanceof HttpErrorResponse && err.status === 401
  }
}