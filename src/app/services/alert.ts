import { Injectable } from '@angular/core';
import { BackendError } from 'types/interfaces';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class Alert {

  constructor(private toastr: ToastrService) { }

  error(reason: BackendError, fallbackMsg: string): void
  error(msg: string): void

  error(reasonOrMsg: BackendError | string, fallbackMsg?: string): void {
    if (reasonOrMsg && typeof reasonOrMsg === 'string') {
      this.toastr.error(reasonOrMsg, 'Error')
    } else {
      let msg = fallbackMsg
      if (reasonOrMsg) {
        const backendError = (reasonOrMsg as BackendError)
        if (backendError.silent) {
          return
        }
        if (backendError.messages) {
          msg = (reasonOrMsg as BackendError).messages.join(', ')
        }
      }
      this.toastr.error(msg, 'Error')
    }
  }

  success(msg: string) {
    this.toastr.success(msg, 'Successful')
  }
}
