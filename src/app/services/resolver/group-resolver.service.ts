import { Injectable } from '@angular/core';
import { GroupedObservable } from 'rxjs';
import { GroupService } from '../api/group.service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GroupResolverService {

  constructor(
    private groupApi: GroupService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.groupApi.group.getMany({})
  }
}
