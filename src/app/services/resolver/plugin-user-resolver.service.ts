import { Injectable } from '@angular/core';
import { PluginUser } from 'types/viewmodels';
import { PluginUserService } from '../api/plugin-user.service';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PluginUserResolverService {

  constructor(
    private pluginUserApi: PluginUserService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.pluginUserApi.pluginUser.getMany({})
  }
  
}
