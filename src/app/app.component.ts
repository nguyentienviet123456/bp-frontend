import { Component, ViewChild, OnInit } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { NgProgress } from '@ngx-progressbar/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd, NavigationError } from '@angular/router';
import { filter, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective
  constructor(
    private toastrService: ToastrService,
    private progressService: NgProgress,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.toastrService.overlayContainer = this.toastContainer;

    const routeEvent = this.router.events

    routeEvent.pipe(
        filter(ev => ev instanceof NavigationStart),
      ).subscribe(() => this.progressService.start())

    routeEvent.pipe(
        filter(ev => ev instanceof NavigationCancel || ev instanceof NavigationEnd || ev instanceof NavigationError),
        debounceTime(50)
      ).subscribe((ev) => {
        this.progressService.done()
      })
  }
}
