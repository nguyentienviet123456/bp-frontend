export interface BpPlugin {
    id: string;
    name: string;
    type: string;
    desc: string;
    targets: string[];
    bg_color: string;
    bg_img: string;
    pkgs: Pkg[];
}

export interface Pkg {
    _id: string,
    name: string,
    exp: string,
    level: string,
    createAt: string,
    updateAt: string
}

export interface User {
    id: string
    email: string;
    name: string;
    picture: string;
    role: string
    createdAt: string
}

export interface EntityResult<T> {
    count: number,
    rows: Array<T>
}

export interface QueryResult<T> {
   entity: EntityResult<T>
}

export interface GetAllResult<T> {
    entity: Array<T>
}

export interface GetResult<T> {
    entity: T
}

export interface PluginUser {
    id: string
    user: User
    plugin: BpPlugin
    members: Member[]
    group: string
    status: string
    form: any
    chatfuel: any
    shop: any
    createAt: any
    updateAt: any
    name: string
}

export interface Member {
    id: string
    email: string
    name: string
    createdAt: string
    role: string
}

export interface Group {
    id: string
    user: User
    name: string
    desc: string
    active: boolean
    createdAt: string
    updatedAt: string
    pluginUsers: PluginUser[]
    weight: number
}

export interface AuthResult {
    token: string
    user: User
}