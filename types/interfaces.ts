export interface BackendError {
    messages?: string[]
    silent?: boolean
  }